export default {
  // Uncomment to enable dark theme
  //dark: true,
  themes: {
    light: {
      primary: "#119939",
      secondary: "#385238",
      tertiary:"#FF0000",
      accent: "#82B1FF",
      error: "#FF5252",
      info: "#003a5d",
      success: "#DB156F",
      warning: "#FFC107"
    },
    dark: {
      primary: "#119939",
      secondary: "#385238",
      tertiary:"#FF0000",
      accent: "#82B1FF",
      error: "#FF5252",
      info: "#003a5d",
      success: "#DB156F",
      warning: "#FFC107"
    }
  }
}
