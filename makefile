

blueDark:=$(shell tput setaf 21)
blue:=$(shell tput setaf 33)

ifeq ($(shell uname),Darwin)
  os=darwin
else
  os=linux
endif

install:
	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache ;\

	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache;\

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)smtp-langres-platform ($(os)): Installing node deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker compose -f docker-compose-builder-$(os).yml run --rm install
	@make -s install-vendor

install-vendor:

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)smtp-langres-platform ($(os)): Installing php deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker compose -f docker-compose-builder-$(os).yml run --rm install-vendor


fixtures:
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)smtp-langres-platform ($(os)): Generating fixtures...)
	$(info $(blue)------------------------------------------------------$(reset))
	@docker compose -f docker-compose-builder-$(os).yml run --rm fixtures

start:
	$(info smtp-langres-platform ($(os)): Starting smtp-langres-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml up -d
 
stop:
	$(info smtp-langres-platform ($(os)): Stopping smtp-langres-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml stop 

status:
	@docker ps -a | grep smtp-langres-platform_platform
	@docker ps -a | grep smtp-langres-platform_db
 
restart:
	$(info smtp-langres-platform ($(os)): Restarting smtp-langres-platform environment containers.)
	@make -s stop
	@make -s start

reload:
	$(info Make ($(os)): Restarting smtp-langres-platform environment containers.)
	@make -s stop
	@make -s remove
	@make -s start

remove:
	$(info smtp-langres-platform ($(os)): Stopping smtp-langres-platform environment containers.)
	@docker compose -f docker-compose-$(os).yml down -v 
 
clean:
	@make -s stop
	@make -s remove
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)Drop all deps + containers + volumes)
	$(info $(blue)------------------------------------------------------$(reset))
	sudo rm -rf node_modules vendor

logs: 
	$(info $(blueDark)------------------------------------------------------)
	$(info $(blueDark)smtp-langres-platform+DB Logs)
	$(info $(blueDark)------------------------------------------------------$(reset))
	@docker logs -f smtp-langres-platform

go-platform:
	@docker exec -it smtp-langres-platform zsh